[
  {
    "id": 1,
    "name": "Звук",
    "type": "bool",
    "location": 1,
    "data": null,
    "update_at": null,
    "validate": {
      "indicators": [
        [
          "success",
          false
        ],
        [
          "danger",
          true
        ]
      ]
    },
    "system": {
      "alertClass": "default",
      "connection": false
    }
  },
  {
    "id": 2,
    "name": "Температура",
    "class": "",
    "type": "number",
    "location": 2,
    "data": null,
    "data_type": "",
    "update_at": null,
    "validate": {
      "min": 5,
      "max": 50,
      "indicators": [
        [
          "danger",
          0,
          24
        ],
        [
          "success",
          25,
          150
        ],
        [
          "danger",
          40,
          255
        ]
      ]
    },
    "system": {
      "alertClass": "default",
      "connection": false
    }
  },
  {
    "id": 3,
    "name": "Вода",
    "type": "bool",
    "location": 1,
    "data": null,
    "update_at": null,
    "validate": {
      "indicators": [
        [
          "info",
          false
        ]
      ]
    },
    "system": {
      "alertClass": "warning",
      "connection": false
    }
  },
  {
    "id": 4,
    "name": "Влажность",
    "type": "number",
    "location": 1,
    "data": null,
    "data_type": "%",
    "update_at": null,
    "system": {
      "alertClass": "default",
      "connection": false
    }
  },
  {
    "id": 5,
    "name": "Газ",
    "type": "bool",
    "location": 2,
    "data": null,
    "update_at": null,
    "system": {
      "alertClass": "default",
      "connection": false
    }
  },
  {
    "id": 6,
    "name": "Вятяжка",
    "class": "",
    "type": "bool",
    "location": 2,
    "data": null,
    "data_type": "",
    "update_at": null,
    "validate": {
      "indicators": [
        [
          "success",
          false
        ],
        [
          "danger",
          true
        ]
      ]
    },
    "system": {
      "alertClass": "default",
      "connection": false
    }
  },
  {
    "id": 7,
    "name": "Окно",
    "class": "",
    "type": "bool",
    "location": 2,
    "data": null,
    "data_type": "",
    "update_at": null,
    "validate": {
      "indicators": [
        [
          "danger",
          false
        ],
        [
          "success",
          true
        ]
      ]
    },
    "system": {
      "alertClass": "default",
      "connection": false
    }
  },
  {
    "id": 8,
    "name": "Яркость света",
    "class": "",
    "type": "number",
    "location": 2,
    "data": null,
    "data_type": "",
    "update_at": null,
    "validate": {
      "min": 5,
      "max": 50,
      "indicators": [
        [
          "warning",
          0,
          24
        ],
        [
          "default",
          25,
          39
        ],
        [
          "warning",
          40,
          60
        ]
      ]
    },
    "system": {
      "alertClass": "default",
      "connection": false
    }
  }
]