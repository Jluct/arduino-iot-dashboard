[
  {
    "id": 6,
    "name": "Шторы",
    "type": "number",
    "location": 2,
    "data": null,
    "actual_data": null,
    "data_type": "%",
    "update_at": null,
    "validate": {
      "min": 0,
      "max": 100
    },
    "system": {
      "alertClass": "default",
      "connection": false
    }
  },
  {
    "id": 7,
    "name": "Вентель горячей воды",
    "type": "checkbox",
    "location": 1,
    "data": null,
    "actual_data": null,
    "data_type": "bool",
    "update_at": null,
    "related": {
      "data": [
        3
      ]
    },
    "validate": {
      "indicators": [
        [
          "success",
          true
        ],
        [
          "danger",
          false
        ]
      ]
    },
    "system": {
      "alertClass": "default",
      "connection": false
    }
  },
  {
    "id": 8,
    "class": "",
    "name": "Концентрация газа",
    "type": "number",
    "data": null,
    "actual_data": null,
    "data_type": "point(s)",
    "update_at": null,
    "location": 1,
    "validate": {
      "min": 0,
      "max": 255,
      "indicators": [
        [
          "success",
          0,
          50
        ],
        [
          "warning",
          50,
          150
        ],
        [
          "danger",
          150,
          255
        ]
      ]
    },
    "system": {
      "alertClass": "default",
      "connection": false
    }
  }
]