angular.module('myApp.home', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'template/home/home.html',
            controller: 'homeCtrl'
        });
    }])

    .controller('homeCtrl', [function () {

    }])
;