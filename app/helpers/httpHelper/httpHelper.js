angular.module('myApp.helpers.httpHelper', [])
    .service('ajaxRequest', ['$http', function ($http) {
        var __private = {
            'getHttp': function (type, url, data) {
                return $http({"method": type, "url": url, "data": data});
            }
        };
        return {
            "ajax": function () {
                return $http;
            },
            "getAjaxComplete": function (url, data, successCallback, errorCallback) {
                data = data || {};
                var http = __private.getHttp('get', url, data);

                if (typeof successCallback === 'function') {
                    http.then(function success(response) {
                        successCallback(response);
                    })
                }
                if (typeof errorCallback === 'function') {
                    http.then(function (e) {
                        errorCallback(e);
                    });
                }
            },
            "getAjax": function (url, param) {
                return this.ajax().get(url, {'params': param});
            },
            "postAjax": function (url, data) {
                return __private.getHttp('POST', url, data);
            },
            "jsonp": function (url, data) {
                return this.ajax().jsonp(url, data);
            }
        };
    }])
;