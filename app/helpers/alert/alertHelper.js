angular.module('myApp.helpers.alertHelper', [])
    .controller('AlertCtrl', ['$scope', 'AppData', function ($scope, AppData) {
        $scope.closeAlert = function (index) {
            AppData.removeAlert(index);
        };
    }])
;