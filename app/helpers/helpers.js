angular.module('myApp.helpers', [])
    .filter('dataType', [function () {
        return function (value, item) {
            if ((!item || value === null || isNaN(value)) && value !== false) {
                return 'load...';
            }

            if (item.type === 'bool' || item.data_type === 'bool') {
                return !!value;
            }

            return item.data + ' ' + item.data_type;
        }
    }])
;