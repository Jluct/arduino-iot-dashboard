angular.module('myApp.helpers.requestHelper', [])
    .factory('loopRequest', ['$interval', function ($interval) {
        var __private = {
            "timer": null,
            "settings": null,
            "callback": null
        };

        return function () {
            return {
                "init": function (settings, callback) {
                    __private.settings = settings;
                    __private.callback = callback;
                    return this;
                },
                "destroy": function () {
                    if (__private.timer) {
                        $interval.cancel(__private.timer);
                    }

                    return this;
                },
                "loop": function (immediately) {
                    if (immediately) {
                        this.run();
                    }
                    __private.timer = $interval(
                        __private.callback,
                        __private.settings.delay,
                        __private.settings.count || 0
                    );
                },
                "run": function () {
                    __private.callback();
                }
            };
        };
    }])
;