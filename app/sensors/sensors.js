angular.module('myApp.sensors', [
    'uiSwitch'
])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/location/', {
            templateUrl: 'template/sensors/sensors.html',
            controller: 'LocationCtrl'
        });
        $routeProvider.when('/location/:id/', {
            templateUrl: 'template/sensors/sensors.html',
            controller: 'LocationCtrl'
        });
    }])
    .controller('SensorsCtrl',
        [
            'LocationFilter',
            'LocationId',
            'AppData',
            'ajaxRequest',
            'loopRequest',
            '$scope',
            function (LocationFilter,
                      LocationId,
                      AppData,
                      ajaxRequest,
                      loopRequest,
                      $scope) {
                if (!LocationId()) {
                    return false;
                }

                AppData.setLocation(LocationId());

                var requestHelper = AppData.getContainerData('timers.sensors');
                if (requestHelper) {
                    requestHelper.destroy();
                }

                $scope.sensors = LocationFilter.filter(AppData.getSensors(), LocationId());
                AppData.setContainerData(
                    'timers.sensors',
                    loopRequest()
                );
                $scope.reverse = false;
                $scope.propertyName = null;

                requestHelper = AppData.getContainerData('timers.sensors');
                requestHelper
                    .init(
                        {"delay": AppData.getAppConfig().settings.query.delay},
                        function () {
                            if (!LocationId()) {
                                return false;
                            }
                            ajaxRequest
                                .getAjax(
                                    AppData.getAppConfig().addresses.HAS,
                                    {"location": AppData.getLocation()}
                                )
                                .then(
                                    function (response) {
                                        // TODO: отрабатывает только GET
                                        AppData.setDataSensors(response.data);
                                    }, function errorCallback(response) {
                                        if (!AppData.getAlert('sensorsError')) {
                                            AppData.addAlert({
                                                'id': 'sensorsError',
                                                'type': 'danger',
                                                'message': "Error! Not connection to " + AppData.getAppConfig().addresses.HAS
                                            })
                                        }
                                    }
                                );
                        }.bind(this)
                    )
                    .loop(true);

                $scope.sortBy = function (propertyName) {
                    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                    $scope.propertyName = propertyName;
                }
            }
        ])
    .controller('DevicesCtrl',
        [
            'LocationId',
            'LocationFilter',
            'AppData',
            'ajaxRequest',
            '$scope',
            function (LocationId,
                      LocationFilter,
                      AppData,
                      ajaxRequest,
                      $scope) {
                if (!LocationId()) {
                    return false;
                }
                $scope.devices = LocationFilter.filter(AppData.getDevices(), LocationId());

                $scope.sendData = function (item) {

                    if (typeof(item.data) === "boolean") {
                        item.data = item.data ? 1 : 0;
                    }

                    ajaxRequest.getAjax(
                        AppData.getAppConfig().addresses.HAS,
                        {"device": item.id, "data": item.data}
                    ).then(
                        function success(response) {
                            if (!response.status) {
                                AppData.addAlert({
                                    'id': 'devicesError',
                                    'type': 'danger',
                                    'message': "Error! Data not set in " + AppData.getAppConfig().addresses.HAS
                                });
                                return;
                            }
                            AppData.setDataDevice(item);
                            AppData.addAlert({
                                'type': 'success',
                                'message': "New data is accepted",
                                'delay': 3000
                            })
                        },
                        function errorCallback(response) {
                            if (!AppData.getAlert('devicesError')) {
                                AppData.addAlert({
                                    'id': 'devicesError',
                                    'type': 'danger',
                                    'message': "Error! Data not set in " + AppData.getAppConfig().addresses.HAS
                                })
                            }
                        }
                    )
                };
            }
        ])
    .controller('LocationCtrl',
        [
            'LocationId',
            'ajaxRequest',
            'AppData',
            '$scope',
            function (LocationId, ajaxRequest, AppData, $scope) {
                AppData.setLocation(LocationId());

                var locations = AppData.getLocations();
                if (locations) {
                    $scope.locations = locations;
                    return;
                }
                ajaxRequest.getAjax('/locations.json')
                    .then(function success(response) {
                        AppData.setLocations(response.data);
                        $scope.locations = response.data;
                    });
            }])
    .factory('LocationFilter', function () {
        return {
            'filter': function (data, id) {
                if (!id) {
                    return null;
                }
                var items = [];
                for (var item in data) {
                    if (data[item].location == id) {
                        items.push(data[item]);
                    }
                }

                return items;
            }
        };
    })
    .filter('trust', ['$sce', function ($sce) {
        return function (val) {
            return $sce.getTrustedHtml(val);
        };
    }])
    .filter('needClass', ['ValidateService', function (validateService) {
        return function (currentClass, item) {
            if (!('validate' in item) || !('indicators' in item.validate) || item.data === undefined) {
                return 'default';
            }

            return validateService[item.type](item.validate.indicators, item.data);
        }
    }])
    .service('ValidateService', [function () {
        return {
            'checkbox': function (indicators, value) {
                for (var key in indicators) {
                    if (indicators.hasOwnProperty(key) && !!indicators[key][1] === !!value) {
                        return indicators[key][0];
                    }
                }

                return 'default';
            },
            'number': function (indicators, value) {
                for (var key in indicators) {
                    if (!indicators.hasOwnProperty(key)) {
                        continue;
                    }
                    var valArray = indicators[key];
                    if (value >= valArray[1] && value <= valArray[2]) {
                        return indicators[key][0];
                    }
                }

                return 'default';
            },
            'range': function (indicators, value) {
                return this.number(indicators, value);
            },
            'bool': function (indicators, value) {
                return this.checkbox(indicators, value);
            }
        }
    }])
    .service('LocationId', ['$routeParams', function ($routeParams) {
        return function () {
            return $routeParams['id'];
        }
    }])
    .service('getDeviceData', ['AppData', 'dataType', function (AppData, dataType) {
        return function (item) {
            return dataType(item);
        }
    }])
;