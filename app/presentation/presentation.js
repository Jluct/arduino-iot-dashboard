angular.module('myApp.presentation', [])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/location', {
            templateUrl: 'sensors/sensors.html',
            controller: 'PresentationCtrl'
        });
    }])
    .controller('PresentationCtrl', ['getPresentations', '$scope', function (getPresentations, $scope) {
        var _self = this;
        getPresentations.getData(function (data) {
            $scope.cards = data;
        }.bind(_self));
    }])
;