angular.module('myApp.presentation.dataHelperService', [])
    .service('getPresentations', ['AppData', 'ajaxRequest', function (AppData, ajaxRequest) {
            return {
                "getData": function (callback) {
                    var data = AppData.getContainerData('presentation');

                    if (data) {
                        callback(data);
                        return;
                    }

                    return ajaxRequest.getAjax('/data/presentation.json', [])
                        .then(
                            function (response) {
                                AppData.setContainerData('presentation', response.data);
                                callback(response.data);
                            }, function errorCallback(response) {
                                if (!AppData.getAlert('sensorsError')) {
                                    AppData.addAlert({
                                        'id': 'dataError',
                                        'type': 'danger',
                                        'message': "Error! No presentation data"
                                    })
                                }
                            }
                        );
                },
                "updateData": function (callback) {
                    ajaxRequest.getAjax('/data/presentation.json', [])
                        .then(
                            function (response) {
                                AppData.setContainerData('presentation', response.data);
                                callback(response.data);
                            }, function errorCallback(response) {
                                if (!AppData.getAlert('sensorsError')) {
                                    AppData.addAlert({
                                        'id': 'dataError',
                                        'type': 'danger',
                                        'message': "Error! No presentation data"
                                    })
                                }
                            }
                        );
                }
            }
        }]
    );