angular.module('myApp.article', [])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/article/:type/', {
            templateUrl: 'template/article/articles.html',
            controller: 'ArticlesCtrl'
        });
        $routeProvider.when('/article/:type/:id', {
            templateUrl: 'template/article/article.html',
            controller: 'ArticleCtrl'
        });
    }])
    .controller('ArticlesCtrl', ['AppData', 'ajaxRequest', '$routeParams', '$scope',
        function (AppData, ajaxRequest, $routeParams, $scope) {
            ajaxRequest.getAjax('/data/' + $routeParams['type'] + '.json', {})
                .then(function success(response) {
                    $scope.articles = response.data;
                });
        }])
    .controller('ArticleCtrl', ['ajaxRequest', '$sce', '$scope', '$routeParams',
        function (ajaxRequest, $sce, $scope, $routeParams) {
            ajaxRequest.getAjax('/data/' + $routeParams['type'] + '.json', {})
                .then(function success(response) {
                    for (var item in response.data) {
                        if (!response.data.hasOwnProperty(item)) {
                            return;
                        }
                        if (response.data[item].id === parseInt($routeParams['id'], 10)) {
                            $scope.article = response.data[item];
                            break;
                        }
                    }

                });
        }])
;