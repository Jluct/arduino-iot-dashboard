// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'ngResource',
    'ui.bootstrap',
    'myApp.article',
    'myApp.presentation',
    'myApp.presentation.dataHelperService',
    'myApp.sensors',
    'myApp.helpers',
    'myApp.helpers.httpHelper',
    'myApp.helpers.requestHelper',
    'myApp.helpers.alertHelper',
    'myApp.home',
    'myApp.initialisation'
])
    .config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
        $routeProvider.otherwise({redirectTo: '/'});
    }])
;

angular.module('myApp.initialisation', [])
/**
 * TODO: Появляющиеся сообщения дёргают экран
 */
    .value('appStorage', {
        "alerts": [],
        "container": {},
        "location": null,
        "locations": null,
        "sensors": null,
        "devices": null,
        "app": null,
        "presentation": null,
        "articles": {
            "instructions": null
        }
    })
    .controller('AppCtrl', ['LocationId', 'AppData', 'ajaxRequest', '$scope', function (LocationId, AppData, ajaxRequest, $scope) {
        ajaxRequest.getAjax('/app.json')
            .then(
                function success(response) {
                    AppData.setAppConfig(response.data);
                },
                function error(response) {
                    AppData.addAlert({
                        'type': 'danger',
                        'message': "Error! No application settings received"
                    })
                });
        ajaxRequest.getAjax('/locations.json', {})
            .then(function success(response) {
                AppData.setLocations(response.data);
            });
        ajaxRequest.getAjax('/sensors.json', {})
            .then(function success(response) {
                AppData.setSensors(response.data);
            });
        ajaxRequest.getAjax('/devices.json', {})
            .then(function success(response) {
                AppData.setDevices(response.data);
            });
        $scope.storage = AppData;

    }])
    .service('AppData', ['appStorage', function (appStorage) {
        return {
            "removeAlert": function (key) {
                appStorage.alerts.splice(key, 1);
            },
            "addAlert": function (data) {
                appStorage.alerts.push(data);
            },
            "getAlerts": function () {
                return appStorage.alerts;
            },
            "getAlert": function (id) {
                var alerts = appStorage.alerts;
                for (var key in alerts) {
                    if (alerts.hasOwnProperty(key) && alerts[key].id === id) {
                        return alerts[key];
                    }
                }

                return null;
            },
            "getContainerData": function (key) {
                if (key in appStorage.container) {
                    return appStorage.container[key];
                }

                return null;
            },
            "setContainerData": function (key, data) {
                appStorage.container[key] = data;
            },
            "getDateFormat": function () {
                return appStorage.app.settings.date_format || 'HH:mm:ss yyyy-MM-dd';
            },
            "setDataSensors": function (data) {
                var _self = this;
                var sensors = _self.getSensors();
                for (var key in sensors) {
                    if (!sensors.hasOwnProperty(key)) {
                        continue;
                    }
                    sensors[key].system.connection = false;
                    for (var item in data) {
                        if (!data.hasOwnProperty(item)) {
                            continue;
                        }
                        if (sensors[key]['id'] === data[item]['id']) {
                            sensors[key].update_at = new Date();
                            sensors[key].system.connection = true;
                            sensors[key] = Object.assign(sensors[key], data[item]);
                        }
                    }
                }
                _self.setSensors(sensors);
            },
            "setDataDevices": function (data) {
                var _self = this;
                var devices = _self.getDevices();
                for (var key in devices) {
                    if (!devices.hasOwnProperty(key)) {
                        continue;
                    }
                    devices[key].system.connection = false;
                    for (var item in data) {
                        if (!data.hasOwnProperty(item)) {
                            continue;
                        }

                        if (devices[key]['id'] === data[item]['id']) {
                            devices[key].update_at = new Date();
                            devices[key].actual_data = data[item]['data'];
                            devices[key].system.connection = true;
                            devices[key] = Object.assign(devices[key], data[item]);
                        }
                    }
                }
                _self.setDevices(devices);
            },
            "setDataDevice": function (item) {
                var _self = this;
                var devices = _self.getDevices();

                for (var key in devices) {
                    if (!devices.hasOwnProperty(key)) {
                        continue;
                    }

                    if (devices[key]['id'] === item['id']) {

                        item.update_at = new Date();
                        item.system.connection = true;
                        item.actual_data = item.data;
                        devices[key] = item;
                    }
                }

            },
            "setAppConfig": function (data) {
                appStorage.app = data;
            },
            "getAppConfig": function () {
                return appStorage.app;
            },
            "setDevices": function (data) {
                appStorage.devices = data;
            },
            "getDevices": function () {
                if ("devices" in appStorage) {
                    return appStorage.devices;
                }

                return null;
            },
            "setSensors": function (data) {
                appStorage.sensors = data;
            },
            "getSensors": function () {
                if ("sensors" in appStorage) {
                    return appStorage.sensors;
                }

                return null;
            },
            "getSensor": function (id) {
                if (!isNumber(id)) {
                    return;
                }

                var _self = this;
                var sensors = _self.getSensors();
                for (var key in sensors) {
                    if (sensors.hasOwnProperty(key) && sensors[key]['id'] === id) {
                        return sensors[key];
                    }
                }
            },
            "setLocation": function (id) {
                appStorage.location = id;
            },
            "getLocation": function () {
                if ('location' in appStorage) {
                    return appStorage.location;
                }

                return null;
            },
            "setLocations": function (data) {
                appStorage.locations = data;
            },
            "getLocations": function () {
                if ('locations' in appStorage) {
                    return appStorage.locations;
                }

                return null;
            }
        }
    }])
;
