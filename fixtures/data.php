<?php
$data = json_decode(trim(file_get_contents('php://input')), true);
header('Content-Type: application/json');

echo json_encode(
    [
        ["id" => 1, "data" => mt_rand(0, 1)],
        ["id" => 2, "data" => mt_rand(0, 255)],
        //["id" => 4, "data" => mt_rand(0, 100)],

    ]
);

die;

switch ($data['location']) {
    case 1:
        echo json_encode(
            [
                ["id" => 1, "data" => mt_rand(0, 1)],
                ["id" => 2, "data" => mt_rand(0, 255)],
                //["id" => 4, "data" => mt_rand(0, 100)],
            
            ]
        );
        break;
    case 2:
        echo json_encode(
            [
                ["id" => 2, "data" => mt_rand(0, 60)],
                ["id" => 5, "data" => mt_rand(0, 1)],
                ["id" => 6, "data" => mt_rand(0, 1)],
                ["id" => 7, "data" => mt_rand(0, 1)],
                ["id" => 8, "data" => mt_rand(0, 60)],
            ]
        );
        break;
}

switch ($data['devices']) {
    case 1:
        echo json_encode(
            [
                ["id" => 7, "data" => mt_rand(0, 1)],
                ["id" => 8, "data" => mt_rand(0, 255)],
            ]
        );
        break;
    case 2:
        echo json_encode(
            [
                ["id" => 6, "data" => mt_rand(0, 100)],
            ]
        );
        break;
}
if (isset($data['devices_data'])) {
    
    switch ($data['id']) {
        case 7:
            echo json_encode(
                ["id" => 7, "data" => $data['data']]
            );
            break;
        case 8:
            echo json_encode(
                ["id" => 8, "data" => $data['data']]
            );
            break;
        case 6:
            echo json_encode(
                ["id" => 6, "data" => $data['data']]
            );
            break;
    }
}