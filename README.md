
# Arduino IoT dashboard

## Содержание
* [Install](#install)
* [App setting](#app_setting)
* [Location list](#location)
* [Sensors setting](#sensors)
* [Devices list](#devices)
* [Note](#note)

-----------------------------------------

## <a name="install">Install</a>

```
    npm install
    node_modules/gulp/bin/gulp.js app:build
    node_modules/gulp/bin/gulp.js app:watch
```

See **`gulpfile.js`** for additional command

-----------------------------------------

## <a name="app_setting">App setting</a>

**Example**
```
{
  "settings": {
    "date_format": "HH:mm:ss yyyy-MM-dd",
    "query": {
        "delay": 3000
    }
  },
  "addresses": {
    "HAS": "",
    "HBS": "",
  }
}
```
* **settings** - содержит основные настройки приложения:
    * *date_format* - формат даты в который будут приведены все даты приложения. По умолчанию "HH:mm:ss yyyy-MM-dd".
    * **query** - настройки запросов к *HAS* и *HBS*.
        * *delay* - задержка между запросами. Значение задаётся в мс. По умолчанию 3000.
* **addresses** - список адресов связи с различными частями приложения:
    * *HAS* - HTTP Ardino Server, IP адрес сервера Ардуино. Используется в случае прямой связи.
    * *HBS* - HTTP Backend Server, адрес сервера обработки данных. Используется в штатном режиме работы приложения.

-----------------------------------------

## <a name="location">Location list</a>

**Example**
```
    [
      {
        "id": 1,
        "name": "Foo"
      },
      {
        "id": 2,
        "name": "Baz"
      }
    ]
```

Каждая локация представленна в виде объекта. Локация может представлять собой любую область:

* **id** - уникальный индификатор помещения или области.
* **name** - название помещения или области.

-----------------------------------------

## <a name="sensors">Sensors setting</a>

**Example**
```
[
  {
    "id": 1,
    "name": "Звук",
    "type": "bool",
    "location": 1,
    "data": null,
    "update_at": null,
    "validate": {
      "indicators": [
        [
          "success",
          false
        ],
        [
          "danger",
          true
        ]
      ]
    },
    "system": {
      "alertClass": "default",
      "connection": false
    }
  },
  {
    "id": 2,
    "name": "Температура",
    "class": "",
    "type": "number",
    "location": 2,
    "data": null,
    "update_at": null,
    "validate": {
      "min": 5,
      "max": 50,
      "indicators": [
        [
          "danger",
          0,
          24
        ],
        [
          "success",
          25,
          39
        ],
        [
          "danger",
          40,
          60
        ]
      ]
    },
    "system": {
      "alertClass": "default",
      "connection": false
    }
  },
```

* **id** - уникальный индификатор датчика.
* **name** - название датчика.
* **type** - тип данных датчика. На данный момент реализовано:
    * *bool* - булево значение;
    * *number* - целое, не отрицательное число;
* **location** - уникальный индификатор локации, к которой принадлежит датчик;
* **data** - данные, полученные с *HAS* или *HBS*;
* **update_at** - время последнего обновления данных;
* **validate** - правила валидации данных:
    * **min** - минимальное ожидаемое значение;
    * **max** - максимальное ожидаемое значение;
    * **indicators** - пороги индикации секции данных датчика в зависимости от полученных значений. 
        Разрешено указывать классы хелперы *bootstap3*. Значения массивов должно быть следующим (см. Note):
        * 0 - название класса хелпера;
        * 1 - минимальный порог;
        * 2 - максимальный порог;
* **system** - системные свойства объекта:  
    * **alertClass** - текущий класс хелпер, который используется секцией датчика;
    * **connection** - состояние соединения с датчиком;
        
-----------------------------------------

## <a name="devices">Devices list</a>

См. [Sesors setting](#sensors)

-----------------------------------------

## <a name="note">Note</a>

* All **id** sensors and delay must have unique number;
* При использовании типа bool необходимо указать только название класса хелпера и значение;
-----------------------------------------