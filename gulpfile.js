'use strict';

var appDir = 'web/';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var imagemin = require('gulp-imagemin');
var del = require('del');
var uglify = require('gulp-uglifyjs');

gulp.task('sass', function () {
    gulp.src('app/assets/sass/**/*.+(css|scss|sass)')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(concat('app.min.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('web/css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('app/assets/sass/**/*.+(css|scss|sass)', ['sass']);
});

gulp.task('html', function () {

    gulp.src([
        'app/**/*.html',
        '!app/index.html',
        '!app/**/example/**/*'
    ])
        .pipe(gulp.dest('web/template'));

    gulp.src('app/index.html')
        .pipe(gulp.dest('web/'));
});

gulp.task('js', function () {

    gulp.src(['app/**/*.js', '!app/bower_components/**/*'])
        .pipe(sourcemaps.init())
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('web/js'));

});

gulp.task('images', function () {
    gulp.src('app/assets/images/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('web/assets/images/'))
});

gulp.task('fonts', function () {
    gulp.src([
        'app/bower_components/bootstrap/dist/fonts/*.+(eot|svg|ttf|woff|woff2)',
        'app/assets/fonts/**/*.+(eot|svg|ttf|woff|woff2)'
    ])
        .pipe(gulp.dest('web/fonts'))
});

gulp.task('app:css:lib', function () {
    gulp.src([
        'app/bower_components/bootstrap/dist/css/bootstrap.min.css',
        'app/bower_components/angular-bootstrap/ui-bootstrap-csp.css',
        'app/bower_components/angular-ui-switch/angular-ui-switch.css'
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('lib.min.css'))
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('web/css'));
});

gulp.task('app:js:lib', function () {
    del('web/js');

    gulp.src([
        'app/bower_components/angular/angular.js',
        'app/bower_components/angular-route/angular-route.js',
        'app/bower_components/angular-loader/angular-loader.js',
        'app/bower_components/angular-resource/angular-resource.js',
        'app/bower_components/angular-bootstrap/ui-bootstrap.js',
        'app/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
        'app/bower_components/angular-ui-switch/angular-ui-switch.js'
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('lib.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('web/js'));

});

gulp.task('app:clean', function () {
    return del.sync('web'); // Удаляем папку dist перед сборкой
});

gulp.task('app:settings', function () {
    gulp.src('settings/**/*.json')
        .pipe(gulp.dest(appDir));
});

gulp.task('app:data', function () {
    gulp.src('data/**/*.+(html|json)')
        .pipe(gulp.dest(appDir + 'data'));
});

gulp.task('app:build', ['app:clean', 'html', 'app:js:lib', 'app:css:lib', 'sass', 'images', 'js', 'fonts', 'app:settings', 'app:data']);


gulp.task('app:watch', function () {
    gulp.watch([
            'data/**/*.+(html|json)'
        ],
        ['app:data']
    );

    gulp.watch('app/assets/sass/**/*.+(css|scss|sass)', ['sass']);
    gulp.watch([
            'app/**/*.html',
            '!/app/**/example/**/*'],
        ['html']
    );
    gulp.watch(
        [
            'app/**/*.js',
            '!app/bower_components/**/*'
        ],
        ['js']
    );
    gulp.watch(
        ['app/assets/images/**/*'],
        ['images']
    );
    gulp.watch(
        [
            'app/bower_components/bootstrap/dist/fonts/*.+(eot|svg|ttf|woff|woff2)',
            'app/assets/fonts/**/*.+(eot|svg|ttf|woff|woff2)'

        ],
        ['fonts']
    );
});